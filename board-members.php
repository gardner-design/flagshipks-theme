<?php
/**
 * Template Name: Board Members
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

/*
$context['board_tax'] = Timber::get_terms( ['taxonomies' => 'board-tax'] );

$context['board_members'] = Timber::get_posts([
	'post_type' => 'board',
	'posts_per_page' => -1,
	'orderby' => 'title',
	'order' => 'ASC'
]);
*/

$templates = ['board-members.twig'];

Timber::render( $templates, $context );