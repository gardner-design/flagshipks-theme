// Shane Scroll | Gardner Design Developer (https://gardnerdesign.com)
(function($) {
    $(document).ready(function() {

		let $body = $('body');

		// basic close function
		$('.close-notice').click(function() {
			$(this).parent().hide();
		});

		// nav shrink function
		if( $(window).width() > 960) {
			$(window).scroll( function() {
				var top_of_object = $('.nav-wrapper').offset().top;

				// after scrolling 50px, shrink nav
				if( top_of_object > 50 ) {
					$('.nav-wrapper').addClass('smaller');
					$('.nav-wrapper').addClass('add-shadow');
					$('.site-logo').addClass('smaller');
				} else {
					$('.nav-wrapper').removeClass('smaller');
					$('.nav-wrapper').removeClass('add-shadow');
					$('.site-logo').removeClass('smaller');
				}
			});
		}

		// navigation functions/handlers
		$(function siteNavigation() {
			$('#menu-toggle').click(function() {
				$('.x-bar').toggleClass('x-bar-active');
				$('.mobile-nav-wrapper').toggleClass('mobile-nav-open');

				// body scroll lock when nav is open
				if( $('.mobile-nav-wrapper').hasClass('mobile-nav-open') ) {
					$body.css('overflow-y', 'hidden');
					$body.css('height', '100%');
					$('.mobile-menu').prepend('<img class="mobile-open-icon" src="/wp-content/themes/flagshipks-theme/static/images/logo-icon-only.svg" alt="Flagship Kansas Tech" />');
					$('.nav-wrapper').css('background-color', '#004cda');
					$('.logo-mobile').css('opacity', '0');
				} else {
					$body.css('overflow-y', 'auto');
					$('.mobile-open-icon').remove(); // clear prepended icon - will create multiple copies of icon if not removed
					$('.nav-wrapper').css('background-color', '#fff'); // white on all pages except homepage
					$('.home .nav-wrapper').css('background-color', '#004cda'); // not white on homepage
					$('.logo-mobile').css('opacity', '1');
				}
			});
		});

		// show a loading symbol for filters once the dataset becomes larger
		$(document).on('facetwp-refresh', function() {
        	$('.facetwp-template').prepend('<div class="is-loading"><span class="gif-loader"></span></div>');
		});

		// once a search is made, filtering turns into a "live filter"
		$(document).on('facetwp-loaded', function() {
			$('.facetwp-template .is-loading').remove();

			// scroll to top of news list after selecting
			if( $('#top').length > 0) {
				var atop = $('#top').offset().top;
				$('html')[0].scrollTo(0, atop - 90);
			}

			if(FWP.refresh) {
				$('.facetwp-template').addClass('visible');
				$('.facetwp-empty-loading').hide();
			} else {
				$('.facetwp-empty-loading').show();
			}
		});

		// accessible accordion block - controls and aria events for screenreaders
		$(function accordionBlock() {
			$('.accordion-content').each(function() {
				$(this).hide();
			});

			$('.accordion-title').click(function() {
				var $this = $(this);
				// fires on first click (content is expanded)
				if( $this.hasClass('target') ) {
					$this.toggleClass('remove-border');
					$this.removeClass('target');
					$this.attr('aria-pressed', 'true');
					$this.next('.accordion-content').slideToggle(700);
					$this.next('.accordion-content').attr('aria-expanded', 'true');
				} else {
					// fires on second click (content is closed)
					$this.next('.accordion-content:first').slideToggle(700, function() {
						$this.prev('.accordion-block-title').addClass('target');
						$this.prev('.accordion-block-title').attr('aria-pressed', 'false');
						$this.attr('aria-expanded', 'false');
						$this.toggleClass('remove-border');
					});
				}
				// always fire
				$(this).toggleClass('chevron-rotate');
			});
		});

		// bbpress subscribe button text change
		$('#subscription-toggle .subscription-toggle').text('Subscribe to this Forum');

	}); // end Document.Ready
})(jQuery);