var windowHeight,
	windowWidth,
	rings;

var videos = [
	{
		id: 'lawn_buddy_video',
		startLoop: 10
	},
	{
		id: 'concergentit_video',
		startLoop: 12
	},
	{
		id: 'goodwill_video',
		startLoop: 19
	},
];

(function($) {
	
    $(document).ready(function() {
	    
	    console.log('hi');
	
		initWalk();
		walk();
		$(window).resize(function() {
			initWalk();
			walk();
		})
		$(document).scroll(function() {
	    	// console.log('scrolling');
			walk();
		});
		initVideos();
		
	});
		
})(jQuery);
	
function initVideos() {
	(function($) {
	
		// Play/pause
		$('.video').click(function() {
	        $(this).toggleClass('playing');
	        
	        $(this).find('video').off('timeupdate'); // Remove looping
	        
			var video = $(this).find('video');
			
			if ($(this).hasClass('playing')) {
				// Play
				
				if ($(this).hasClass('not_played')) {
					video[0].currentTime = 0; // Start from beginning
					$(this).removeClass('not_played');
				}
				
	            video.prop('muted', false);
	            video.prop('loop', false);
	            video[0].play();
	        } else {
		        // Pause
	            video[0].pause();
	        }
	        
		});
		$('video').on('ended',function() {
	    	$(this).parents('.video').removeClass('playing');
			// video[0].currentTime = 0; // Start from beginning
			
			var i = $(this).parents('.video').index();
			startVideoLoop(videos[i]);
			
	    });
		$('.video .replay').click(function(e) {
	        var video = $(this).parents('.video').find('video');
	        video[0].currentTime = 0; // Start from beginning
	        video[0].play();
	    });
	    
	    // Start all videos looping, within range
	
		for (i = 0; i < videos.length; i++) {
			var video = videos[i];
				
			startVideoLoop(video);
			
		}
	
	})(jQuery);
}
function startVideoLoop(video) {
	(function($) {
	
		var videoEl = $('#' + video.id);
		
	    videoEl.prop('muted', true);
	    videoEl.prop('loop', true);
		videoEl[0].currentTime = video.startLoop;
	    videoEl[0].play();
		videoEl.on('timeupdate',function() {
			if (this.currentTime > video.startLoop + 5) {
				this.currentTime = video.startLoop;
			}
		});
		
		videoEl.parents('.video').addClass('not_played');
	
	})(jQuery);
}

function initWalk() {
	(function($) {
		
		windowHeight = $(window).height();
		windowWidth = $(window).width();
		rings = $('.rings');
		
	})(jQuery);
}
function walk() {
	(function($) {
		
		var scrollFromTop = $(document).scrollTop();
		
		// console.log(scrollFromTop);
		
		$('.scroll_down_button').toggleClass('hidden', (scrollFromTop > 0));
		
		// Animate rings
		
		if (windowWidth > 600) {
			if (scrollFromTop <= windowHeight * 2) { // if within top sections
			
				if ($('.rings').hasClass('animate_exit')) {
					$('.rings').removeClass('animate_exit');
				}
				 
				// Animate rings with scroll
				var s = 1 + (scrollFromTop / windowHeight * 0.4);
				
				var css = '@media ( min-width: 601px ) { .rings svg { transform: scale(' + s + '); } }'; // translateY(-' + y + 'px) 
				addCss('rings_style', css);
			} else {
				
				addCss('rings_style', '@media ( min-width: 601px ) { .rings svg { transform: scale(1.8); } }'); // max scale
			
			}
		}
		
		var strategyEndFromTop = $('.strategy_outer').offset().top + $('.strategy_outer').outerHeight();
		
		if (scrollFromTop > strategyEndFromTop - (windowHeight * 0.8)) { // if past Strategy section
			
			if (!$('.rings').hasClass('animate_exit')) {
				$('.rings').addClass('animate_exit');
			}
			
		} else {
			
			if ($('.rings').hasClass('animate_exit')) {
				$('.rings').removeClass('animate_exit');
			}
			
		}
		
		// Animate horizontal scroll of Strategy section
		
		var el = $('.strategy_outer');
		var elFromTop = el.offset().top,
			elHeight = el.height(),
			elInnerWidth = windowWidth * 3; // :) $('.strategy').outerWidth();
		
		var within = ((scrollFromTop - elFromTop) / (elHeight - windowHeight)).toFixed(6);
		
		var l = -(elInnerWidth - windowWidth) * within;
		
		// console.log(l);
		
		if (l > 0) {
			l = 0;
		} else if (l < -elInnerWidth + windowWidth) {
			l = -elInnerWidth + windowWidth;
		}
		
		// console.log(l);
		
		addCss('strategy_style', '@media ( min-width: 601px ) { .strategy section { transform: translateX(' + (Math.round(l)) + 'px); } }');

	})(jQuery);
}

var styles = [];
function addCss(id, css) {
	if (styles[id] == null) {
        var el = document.createElement('style');
        el.type = 'text/css';
		document.getElementsByTagName('body')[0].appendChild(el);
		styles[id] = el;
    }
    styles[id].innerHTML = css;
}