<?php
// This functions file is for all custom blocks added via ACF
// Reference: https://www.advancedcustomfields.com/resources/acf_register_block_type/

if( function_exists('acf_register_block_type') ) :
	include 'acf-blocks-callback.php'; // pass-off to let Timber render the blocks

	// accessible and dynamic accordion block
	$accordion_block = [
		'name' => 'accordion-block',
		'title' => __( 'Accordion Block', 'mod' ),
		'description' => __( 'Creates an accordion; The content is folded into the title.', 'mod' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'mod-blocks',
		'align' => 'wide',
		'icon' => 'editor-kitchensink',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'drop', 'down', 'dropdown', 'accordion' ]
	];
	acf_register_block_type( $accordion_block );

	// yellow button block
	$yellow_button = [
		'name' => 'yellow-button',
		'title' => __( 'Yellow Button', 'mod' ),
		'description' => __( 'Creates a button with a yellow background and blue text.', 'mod' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'mod-blocks',
		'align' => 'center',
		'icon' => 'button',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'yellow', 'button', 'link' ]
	];
	acf_register_block_type( $yellow_button );

	// blue button block
	$blue_button = [
		'name' => 'blue-button',
		'title' => __( 'Blue Button', 'mod' ),
		'description' => __( 'Creates a button with a blue background and white text.', 'mod' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'mod-blocks',
		'align' => 'center',
		'icon' => 'button',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'blue', 'button', 'link' ]
	];
	acf_register_block_type( $blue_button );

	// cyan button block
	$cyan_button = [
		'name' => 'cyan-button',
		'title' => __( 'Cyan Button', 'mod' ),
		'description' => __( 'Creates a button with a cyan background and white text.', 'mod' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'mod-blocks',
		'align' => 'center',
		'icon' => 'button',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'cyan', 'button', 'link' ]
	];
	acf_register_block_type( $cyan_button );

	// strategy block
	$strategy_block = [
		'name' => 'strategy-block',
		'title' => __( 'Strategy Block', 'mod' ),
		'description' => __( 'Creates a block with the 3 strategies. Awareness, Education, Workforce.', 'mod' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'mod-blocks',
		'align' => 'wide',
		'icon' => 'block-default',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'strategy', 'strategies', 'block', 'awareness', 'education', 'workforce', '3', 'three' ]
	];
	acf_register_block_type( $strategy_block );

	// video stories block
	$video_stories = [
		'name' => 'video-stories',
		'title' => __( 'Video Stories', 'mod' ),
		'description' => __( 'Creates a full width block with alternating videos left and right.', 'mod' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'mod-blocks',
		'align' => 'wide',
		'icon' => 'video-alt2',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'video', 'videos', 'story', 'stories', 'block', 'left', 'right', 'alternating' ]
	];
	acf_register_block_type( $video_stories );
	
	// New blocks added by Jay
    acf_register_block_type([
        'name'              => 'board-members',
        'title'             => __('Board Members'),
		'render_callback'	=> 'acf_custom_blocks_callback',
		'category'			=> 'mod-blocks',
        'icon'              => 'groups',
        'post_types'		=> ['page'],
		'mode' => 'auto',
		'supports' => [ 'mode' => true ]
    ]);
    acf_register_block_type([
        'name'              => 'sponsors-block',
        'title'             => __('Sponsors'),
		'render_callback'	=> 'acf_custom_blocks_callback',
		'category'			=> 'mod-blocks',
        'icon'              => 'groups',
        'post_types'		=> ['page'],
		'mode' => 'auto',
		'supports' => [ 'mode' => true ]
    ]);
	
endif;