<?php

// This is the callback that renders the blocks created in [acf-block-functions.php]
function acf_custom_blocks_callback( $block, $content = '', $is_preview = false ) {
	$context = Timber::get_context();

	$context['block'] = $block;
	$context['fields'] = get_fields();
	$context['is_preview'] = $is_preview;
	
	$context['board_tax'] = Timber::get_terms( ['taxonomies' => 'board-tax'] );
	
	$context['governing_board'] = Timber::get_posts([
		'post_type' => 'board',
		'posts_per_page' => -1,
		'orderby' => 'menu_order',
		'order' => 'ASC',
        'tax_query' => array(
            array(
                'taxonomy' => 'board-tax',
                'field' => 'slug',
                'terms' => 'governing-board'
            )
        )
	]);
	$context['advisory_board'] = Timber::get_posts([
		'post_type' => 'board',
		'posts_per_page' => -1,
		'orderby' => 'menu_order',
		'order' => 'ASC',
        'tax_query' => array(
            array(
                'taxonomy' => 'board-tax',
                'field' => 'slug',
                'terms' => 'advisory-board'
            )
        )
	]);
	
	$context['sponsor_terms'] = Timber::get_terms([
		'taxonomies' => 'sponsor-tax'
	]);
	$context['sponsors'] = Timber::get_posts([
		'post_type' => 'sponsor',
		'posts_per_page' => -1,
		'orderby' => 'title',
		'order' => 'ASC'
	]);

/*
	$context['board_members'] = Timber::get_posts([
		'post_type' => 'board',
		'posts_per_page' => -1,
		'orderby' => 'title',
		'order' => 'DESC'
	]);
*/

	// templates/blocks/acf/BLOCK_NAME.twig
	$template = 'templates/blocks/' . $block['name'] . '.twig';
	Timber::render( $template, $context );
}