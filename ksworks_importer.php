<?php
/**
 * Author: Shane Schroll
 * Description: Importer and cleanup function for KSWorks Jobs Import (data returned is JSON)
*/

// clean up job entries daily
add_action( 'flagship_job_cleanup', 'job_import_cleanup' );
function job_import_cleanup() {

	// delete all current job postings for the day (new job list imported 15 minutes later)
	$reset_posts = get_posts([
		'post_type' => 'job',
		'posts_per_page' => -1,
	]);

	foreach( $reset_posts as $rps ) {
		wp_delete_post( $rps->ID, true );
	}
}

// Imports up to 10k Jobs (Max available on KSWorks)
add_action( 'flagship_job_import', 'ksworks_job_import' );
function ksworks_job_import() {

	$max_page = 200;

	for( $current_page = 1; $current_page <= $max_page; $current_page++ ) {

		$url = 'https://www.kansasworks.com/search/jobs.json?page=' . $current_page . '&per_page=50&search_job_search[keywords]=technology';

		$get_jobs = wp_remote_get( $url );

		if( is_wp_error( $get_jobs ) ) {
			return false;
		}

		$jobs = json_decode( wp_remote_retrieve_body( $get_jobs ), true );

		// loop through jobs on each page
		foreach( $jobs['jobs'] as $key => $val ) :
			$job = $jobs['jobs'][$key]; // raw key value returns an index number (index pointer)

			// $val array pointers:  id, url, title, updated_on, description, employer, location, zip_code
			if( !empty($job['url']) ) {
				$url = $job['url'];
			}

			if( !empty($job['title']) ) {
				$title = $job['title'];
			}

			if( !empty($job['description']) ) {
				$description = $job['description'];
			}

			if( !empty($job['coordinate'][0]) && isset($job['coordinate'][0]) ) {
				$latitude = $job['coordinate'][0];
			}

			if( !empty($job['coordinate'][1]) && isset($job['coordinate'][1]) ) {
				$longitude = $job['coordinate'][1];
			}

			if( !empty($job['employer']) ) {
				$employer = $job['employer'];
			} else {
				$employer = 'No Employer Data';
			}

			// extract City and State from the location
			if( !empty($job['location']) ) {
				$location = $job['location'];
				preg_match( '/(,[A-Za-z\s]+,\s[A-Za-z]+\s)/', $location, $match );
				$city_state = $match[0] ?? '  No Location Data';
			}

			// save data into custom fields inside the 'job' post type
			$job_post = [
				'post_type'   => 'job',
				'post_title'  => $title,
				'post_status' => 'publish'
			];

			$job_id_key = 'field_60f8934a8edb4';
			$job_url_key = 'field_60f893598edb5';
			$job_desc_key = 'field_60f893718edb7';
			$job_emp_key = 'field_60f893928edb8';
			$job_loc_key = 'field_60f8939a8edb9';
			$job_lat_long = 'field_611292bf0ca81';

			$post_id = wp_insert_post( $job_post );

			// This updates our ACF fields with the sorted JSON data
			update_field( $job_url_key, $url, $post_id ); // Job URL
			update_field( $job_desc_key, $description, $post_id ); // Job Description
			update_field( $job_emp_key, $employer, $post_id ); // Job Employer
			update_field( $job_loc_key, substr( $city_state, 2), $post_id ); // Job Location
			update_field( $job_lat_long, $latitude . ', ' . $longitude, $post_id ); // Job Latitude/Longitude
		endforeach;
	}
}