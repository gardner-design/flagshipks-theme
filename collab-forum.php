<?php
/**
 * Template Name: Forum Template
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

// Forum is for collaboration between internal/staff members only.
// Public registration is not allowed. Forum is only visible to logged-in users
$templates = ['collab-forum.twig'];

Timber::render( $templates, $context );