<?php

// change 'views' directory to 'templates'
Timber::$locations = __DIR__ . '/templates';

class MODSite extends TimberSite {

	function __construct() {
		// Action Hooks //
		add_action( 'after_setup_theme', [ $this, 'after_setup_theme' ] );
		add_action( 'wp_enqueue_scripts', [ $this, 'enqueue_scripts' ] );
		add_action( 'admin_head', [ $this, 'admin_head_css' ] );
		add_action( 'admin_menu', [ $this, 'admin_menu_cleanup'] );
		add_action( 'admin_menu', [ $this, 'global_site_options_menu' ] );
		add_action( 'init', [ $this, 'register_post_types' ] );
		add_action( 'login_enqueue_scripts', [ $this, 'style_login' ] );
		add_action( 'acf/init', [ $this, 'render_custom_acf_blocks' ] );

		// Filter Hooks //
		add_filter( 'timber_context', [ $this, 'add_to_context' ] );
		add_filter( 'block_categories', [ $this, 'mod_block_category' ], 10, 2 );
		add_filter( 'manage_pages_columns', [ $this, 'remove_pages_count_columns'] );

		parent::__construct();
	}

	// hide WP update nag (most hosts control this)
	function admin_head_css() {
		?>
		<style type="text/css">
			.update-nag { display: none !important; }
			#wp-admin-bar-comments { display: none !important; }

			#menu-posts-news .menu-top {
				margin-top: 1rem !important;
				padding-top: 1rem !important;
				border-top: 1px solid #616161 !important;
			}

			#menu-posts-job .menu-top {
				margin-bottom: 0.6rem !important;
				padding-bottom: 1.1rem !important;
				border-bottom: 1px solid #616161;
			}
		</style>
		<?php
	}

	// WP admin login styles
	function style_login() {
		?>
		<style type="text/css">
			#login h1, .login h1 {
				background-color: white;
				padding: 1.5rem 0.5rem;
				border-radius: 2px;
			}

			#login h1 a, .login h1 a {
				background-image: url('<?= get_stylesheet_directory_uri() . '/static/images/logo-white.svg' ?>') !important;
				background-position: center;
				width: 10rem;
				background-size: contain;
				padding: 1rem;
				margin: 0 auto;
			}
		</style>
		<?php
	}

	// enqueue styles & scripts
	function enqueue_scripts() {
		$version = filemtime( get_stylesheet_directory() . '/style-dist.css' );
		wp_enqueue_style( 'mod-css', get_stylesheet_directory_uri() . '/style-dist.css', [], $version );
		wp_enqueue_style( 'block-css', get_stylesheet_directory_uri() . '/block-style-dist.css', [], $version );
		wp_enqueue_script( 'mod-js', get_template_directory_uri() . '/static/js/site-dist.js', ['jquery'], $version );

		if ( is_front_page() || get_the_ID() == 354 ) {
			wp_enqueue_script( 'front-js', get_template_directory_uri() . '/static/js/front-page.js', ['jquery'], $version );
		}
	}

	// Custom context helper functions (callable)
	function add_to_context( $context ) {
		$context['site']           = $this;
		$context['date']           = date( 'F j, Y' );
		$context['date_year']      = date( 'Y' );
		$context['options']        = get_fields( 'option' );
		$context['home_url']       = home_url( '/' );
		$context['is_front_page']  = is_front_page();
		$context['get_url']        = $_SERVER['REQUEST_URI'];

		return $context;
	}

	// Menus / Theme Support / ACF Options Page
	function after_setup_theme() {
		register_nav_menu( 'primary', 'Main Nav' );
		register_nav_menu( 'footer', 'Footer Nav' );

		add_theme_support( 'menus' );
		add_theme_support( 'align-wide' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'editor-styles' );
		add_theme_support( 'disable-custom-colors' ); // disable color picker wheel
		
		// Add support for editor color palette
		
		add_theme_support('editor-color-palette', array(
			array(
				'name'  => __('Blue', 'flagship'),
				'slug'  => 'flagship-blue',
				'color'	=> '#004CDA'
			),
			array(
				'name'  => __('Dark Blue', 'flagship'),
				'slug'  => 'flagship-blue-dark',
				'color'	=> '#003891'
			),
			array(
				'name'  => __('Cyan', 'flagship'),
				'slug'  => 'flagship-cyan',
				'color'	=> '#00C5EC'
			),
			array(
				'name'  => __('Gold', 'flagship'),
				'slug'  => 'flagship-gold',
				'color'	=> '#FFBC00'
			)
		));

		add_editor_style( 'block-style-dist.css' );

		// create Global Options page for things like footer data and company info/logos
		if( function_exists( 'acf_add_options_page' ) ) {
			$parent = acf_add_options_page([
				'page_title'      => 'Theme Options',
				'menu_title'      => 'Theme Options',
				'capability'      => 'edit_posts',
				'redirect'        => false,
				'updated_message' => 'Updated.',
			]);

			// Notification Banner
			$child = acf_add_options_sub_page([
				'page_title'  => __( 'Notification Banner' ),
				'menu_title'  => __( 'Notice Banner' ),
				'parent_slug' => $parent['menu_slug'],
			]);

			// Contact Settings
			$child = acf_add_options_sub_page([
				'page_title'  => __( 'Contact Settings' ),
				'menu_title'  => __( 'Contact' ),
				'parent_slug' => $parent['menu_slug'],
			]);

			// Social Settings
			$child = acf_add_options_sub_page([
				'page_title'  => __( 'Social Settings' ),
				'menu_title'  => __( 'Social' ),
				'parent_slug' => $parent['menu_slug'],
			]);

			// 404 Settings
			$child = acf_add_options_sub_page([
				'page_title'  => __( '404 Settings' ),
				'menu_title'  => __( '404' ),
				'parent_slug' => $parent['menu_slug'],
			]);
		}
	}

	// registers and renders our custom acf blocks
	function render_custom_acf_blocks() {
		require 'acf-block-functions.php';
	}

	// creates a custom block category for our theme-specific blocks
	function mod_block_category( $categories, $post ) {
		return array_merge(
			$categories, [
				[
					'slug'  => 'mod-blocks',
					'title' => 'Custom Blocks',
				],
			]
		);
	}

	// include post types
	function register_post_types() {
		include_once('custom-post-types/post-type-news.php');
		include_once('custom-post-types/post-type-board.php');
		include_once('custom-post-types/post-type-sponsor.php');
		include_once('custom-post-types/post-type-member.php');
		include_once('custom-post-types/post-type-job.php');
	}

	// remove unused items from admin menu
	function admin_menu_cleanup() {
		remove_menu_page( 'edit.php' ); // Posts
		remove_menu_page( 'edit-comments.php' ); // Comments
	}

	// removed comment column from posts pages
	function remove_pages_count_columns( $defaults ) {
		unset($defaults['comments']);
		return $defaults;
	}
} // End of MODSite class

new MODSite();

// main site nav (used to render the menu in /templates/navigation.twig)
function mod_render_primary_menu() {
	wp_nav_menu([
		'theme_location' => 'primary',
		'container'      => false,
		'menu_id'        => 'primary-menu',
	]);
}

function mod_render_footer_menu() {
	wp_nav_menu([
		'theme_location' => 'footer',
		'container'      => false,
		'menu_id'        => 'footer-menu',
	]);
}

// disable the block editor on certain templates
function mod_disable_editor( $id = false ) {
	$excluded_templates = [
		'front-page.php',
		'jobs-archive.php'
	];

	if( empty( $id ) )
		return false;

	$id = intval( $id );
	$template = get_page_template_slug( $id );

	return in_array( $template, $excluded_templates );
}

function mod_disable_gutenberg( $can_edit, $post_type ) {
	if( ! ( is_admin() && !empty( $_GET['post'] ) ) )
		return $can_edit;

	if( mod_disable_editor( $_GET['post'] ) )
		$can_edit = false;

	return $can_edit;
}
add_filter( 'gutenberg_can_edit_post_type', 'mod_disable_gutenberg', 10, 2 );
add_filter( 'use_block_editor_for_post_type', 'mod_disable_gutenberg', 10, 2 );

// 1. Old Job Cleanup function (CRON: 12:00 AM)
// 2. KSworks job importer functions (CRON: 1:15 AM)
// 3. Taxonomy assignment function (CRON: 1:20 AM)
include( 'ksworks_importer.php' );

// Add edit page control to pages when logged in

function remove_admin_bar() {
	show_admin_bar(false);
}
add_action('after_setup_theme', 'remove_admin_bar');

function gdusa_admin_control() {
	if (is_user_logged_in() && !empty(get_edit_post_link())) {
		echo '<style>#gdusa-edit_post_button{position:fixed;z-index:300;bottom:0;right:0;width:50px;height:50px;color:transparent;font-size:0;text-indent:-1000em;text-decoration:none;outline:none;overflow:visible;cursor:pointer}#gdusa-edit_post_button:before{content:"";position:absolute;z-index:-1;top:0;left:0;width:0;height:0;border:solid 57px transparent;border-top-color:rgba(0,0,0,0.2);transform-origin:0 0;transform:rotate(-45deg) translate(-57px,14px);transition:border 0.3s;pointer-events:none}#gdusa-edit_post_button:after{content:"";position:absolute;top:15px;left:15px;width:20px;height:20px;background-repeat:no-repeat;background-position:0 0;background-size:100% auto;background-image:url("data:image/svg+xml,%3C%3Fxml version=\'1.0\' encoding=\'utf-8\'%3F%3E%3C!-- Generator: Adobe Illustrator 24.2.1, SVG Export Plug-In . SVG Version: 6.00 Build 0) --%3E%3Csvg version=\'1.1\' id=\'Layer_1\' focusable=\'false\' xmlns=\'http://www.w3.org/2000/svg\' xmlns:xlink=\'http://www.w3.org/1999/xlink\' x=\'0px\' y=\'0px\' viewBox=\'0 0 20 20\' style=\'enable-background:new 0 0 20 20;\' xml:space=\'preserve\'%3E%3Cstyle type=\'text/css\'%3E .st0%7Bfill:%23FFFFFF;%7D%0A%3C/style%3E%3Cpath class=\'st0\' d=\'M16.3,3.7c-0.9-0.9-2.3-0.9-3.1,0l-0.5,0.6L2.5,14.4v3.1h3.1L15.7,7.4l0.6-0.5C17.1,6,17.1,4.6,16.3,3.7z M15.1,5.7l-1.8,1.8l-2,2l-5.9,5.9l-1.8,1l1-1.8l5.9-5.9l2-2l1.8-1.8c0.2-0.2,0.6-0.2,0.8,0S15.3,5.5,15.1,5.7z\'/%3E%3C/svg%3E%0A")}#gdusa-edit_post_button:hover:before{border-top-color:#1e72a7}.gdusa-edit_link{display:block;float:right;width:44px;height:44px;font-size:0;padding:0;border-radius:50px;background-color:rgba(0,0,0,0.2);background-repeat:no-repeat;background-position:50% 50%;background-size:20px auto;background-image:url("data:image/svg+xml,%3C%3Fxml version=\'1.0\' encoding=\'utf-8\'%3F%3E%3C!-- Generator: Adobe Illustrator 24.2.1, SVG Export Plug-In . SVG Version: 6.00 Build 0) --%3E%3Csvg version=\'1.1\' id=\'Layer_1\' focusable=\'false\' xmlns=\'http://www.w3.org/2000/svg\' xmlns:xlink=\'http://www.w3.org/1999/xlink\' x=\'0px\' y=\'0px\' viewBox=\'0 0 20 20\' style=\'enable-background:new 0 0 20 20;\' xml:space=\'preserve\'%3E%3Cstyle type=\'text/css\'%3E .st0%7Bfill:%23FFFFFF;%7D%0A%3C/style%3E%3Cpath class=\'st0\' d=\'M16.3,3.7c-0.9-0.9-2.3-0.9-3.1,0l-0.5,0.6L2.5,14.4v3.1h3.1L15.7,7.4l0.6-0.5C17.1,6,17.1,4.6,16.3,3.7z M15.1,5.7l-1.8,1.8l-2,2l-5.9,5.9l-1.8,1l1-1.8l5.9-5.9l2-2l1.8-1.8c0.2-0.2,0.6-0.2,0.8,0S15.3,5.5,15.1,5.7z\'/%3E%3C/svg%3E%0A");margin-top:-22px;margin-bottom:-22px;margin-left:-22px;margin-right:-22px;transition:background 0.3s}.gdusa-edit_link:hover{background-color:#1e72a7}</style>
<a id="gdusa-edit_post_button" href="' . get_edit_post_link() . '" target="_blank" rel="nofollow" title="Edit ' . ucfirst(get_post_type()) . '">Edit ' . ucfirst(get_post_type()) . '</a>';
	}
}
add_action('wp_footer', 'gdusa_admin_control');