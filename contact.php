<?php
/**
 * Template Name: Contact Template
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$templates = ['contact.twig'];

Timber::render( $templates, $context );