<?php
/**
 * Template Name: Home Template
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$context['sponsor_terms'] = Timber::get_terms( ['taxonomies' => 'sponsor-tax'] );

$context['sponsors'] = Timber::get_posts([
	'post_type' => 'sponsor',
	'posts_per_page' => -1,
	'orderby' => 'title',
	'order' => 'ASC'
]);

$templates = ['front-page.twig'];

Timber::render( $templates, $context );