To modify any portion of the bbpress forum plugin, you must copy the template file you want to edit into this directory.
This directory is the "child-plugin" folder and any files copied and modified here will override the default bbpress templates.

You can find the templates in    \wp-content\plugins\bbpress\templates\default\bbpress

To override or modify styles, use the _forum.scss file in this theme.    \flagshipks-theme\static\scss\_forum.scss

User Roles within the forum
https://codex.bbpress.org/getting-started/before-installing/bbpress-user-roles-and-capabilities/

Base Settings
https://codex.bbpress.org/getting-started/configuring-bbpress/forum-settings/

Note that the documentation is half-baked and incomplete. You will have to Google a lot.