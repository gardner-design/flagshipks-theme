<?php
/**
 * No Topics Feedback Part
 *
 * @package bbPress
 * @subpackage Theme
*/

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;
?>
<div class="bbp-template-notice">
	<ul>
		<li><?php esc_html_e( 'There are currently no Topics. Use the form below to create one.', 'bbpress' ); ?></li>
	</ul>
</div>