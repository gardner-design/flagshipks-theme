<?php
/**
 * No Forums Feedback Part
 *
 * @package bbPress
 * @subpackage Theme
*/

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;
?>
<div class="bbp-template-notice">
	<ul>
		<li><?php esc_html_e( 'You must be logged in and a member to access the forums.', 'bbpress' ); ?></li>
	</ul>
</div>