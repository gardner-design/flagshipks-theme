<?php
/**
 * Template Name: Jobs Archive
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$context['job_tax'] = Timber::get_terms( ['taxonomies' => 'job-tax'] );

$context['job_posts'] = Timber::get_posts([
	'post_type' => 'job',
	'posts_per_page' => 20,
	'orderby' => 'title',
	'order' => 'ASC',
	'facetwp' => true
]);

$templates = ['jobs-archive.twig'];

Timber::render( $templates, $context );