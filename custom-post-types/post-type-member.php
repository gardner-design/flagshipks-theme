<?php
$labels = [
	'name'                => __( 'Members', 'mod' ),
	'singular_name'       => __( 'Member', 'mod' ),
	'add_new'             => _x( 'Add Member', 'mod', 'mod' ),
	'add_new_item'        => __( 'Add Member', 'mod' ),
	'edit_item'           => __( 'Edit Member', 'mod' ),
	'new_item'            => __( 'Add Member', 'mod' ),
	'view_item'           => __( 'View Member', 'mod' ),
	'search_items'        => __( 'Search Members', 'mod' ),
	'not_found'           => __( 'No Members found', 'mod' ),
	'not_found_in_trash'  => __( 'No Members found in Trash', 'mod' ),
	'parent_item_colon'   => __( 'Parent Member:', 'mod' ),
	'menu_name'           => __( 'Members', 'mod' ),
];

$args = [
	'labels'              => $labels,
	'hierarchical'        => false,
	'description'         => 'Membership holders.',
	'taxonomies'          => [ 'member-tax' ],
	'public'              => false,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'show_in_rest'		  => true,
	'menu_position'       => null,
	'menu_icon'           => 'dashicons-groups',
	'show_in_nav_menus'   => true,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'has_archive'         => false,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => [ 'title', 'thumbnail', 'author' ]
];

register_post_type( 'member', $args );

// setup taxonomies
$tax_labels = [
	'name' 				=> _x( 'Member Categories', 'mod' ),
	'singular_name' 	=> _x( 'Member Category', 'mod' ),
	'search_items' 		=> __( 'Search Member Categories', 'mod' ),
	'all_items' 		=> __( 'All Member Categories', 'mod' ),
	'edit_item' 		=> __( 'Edit Member Category', 'mod' ),
	'update_item' 		=> __( 'Update Member Category', 'mod' ),
	'add_new_item' 		=> __( 'Add Member Category', 'mod' ),
	'new_item_name' 	=> __( 'Create Member Category', 'mod' ),
	'menu_name' 		=> __( 'Member Categories', 'mod' ),
	'parent_item'		=> __( 'Category Parent', 'mod' ),
];

$tax_args = [
	'hierarchical' 	    => true,
	'labels' 	    	=> $tax_labels,
	'show_ui' 	    	=> true,
	'show_admin_column' => true,
	'has_archive'		=> false,
	'query_var'	    	=> true,
	'show_in_rest'		=> true,
	'rewrite'			=> true,
];
register_taxonomy( 'member-tax', 'member', $tax_args );