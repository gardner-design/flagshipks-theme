<?php
$labels = [
	'name'                => __( 'Jobs', 'mod' ),
	'singular_name'       => __( 'Job', 'mod' ),
	'add_new'             => _x( 'Add Job', 'mod', 'mod' ),
	'add_new_item'        => __( 'Add Job', 'mod' ),
	'edit_item'           => __( 'Edit Job', 'mod' ),
	'new_item'            => __( 'Add Job', 'mod' ),
	'view_item'           => __( 'View Job', 'mod' ),
	'search_items'        => __( 'Search Jobs', 'mod' ),
	'not_found'           => __( 'No Jobs found', 'mod' ),
	'not_found_in_trash'  => __( 'No Jobs found in Trash', 'mod' ),
	'parent_item_colon'   => __( 'Parent Job:', 'mod' ),
	'menu_name'           => __( 'Jobs', 'mod' ),
];

$args = [
	'labels'              => $labels,
	'hierarchical'        => false,
	'description'         => 'Job.',
	'taxonomies'          => [ 'job-tax' ],
	'public'              => false,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => false,
	'show_in_rest'		  => true,
	'menu_position'       => null,
	'menu_icon'           => 'dashicons-nametag',
	'show_in_nav_menus'   => true,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'has_archive'         => false,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => [ 'title', 'editor', 'thumbnail', 'page-attributes' ]
];

register_post_type( 'job', $args );

// setup taxonomies
$tax_labels = [
	'name' 				=> _x( 'Job Categories', 'mod' ),
	'singular_name' 	=> _x( 'Job Category', 'mod' ),
	'search_items' 		=> __( 'Search Job Categories', 'mod' ),
	'all_items' 		=> __( 'All Job Categories', 'mod' ),
	'edit_item' 		=> __( 'Edit Job Category', 'mod' ),
	'update_item' 		=> __( 'Update Job Category', 'mod' ),
	'add_new_item' 		=> __( 'Add Job Category', 'mod' ),
	'new_item_name' 	=> __( 'Create Job Category', 'mod' ),
	'menu_name' 		=> __( 'Job Categories', 'mod' ),
	'parent_item'		=> __( 'Category Parent', 'mod' ),
];

$tax_args = [
	'hierarchical' 	    => true,
	'labels' 	    	=> $tax_labels,
	'show_ui' 	    	=> true,
	'show_admin_column' => true,
	'has_archive'		=> false,
	'query_var'	    	=> true,
	'show_in_rest'		=> true,
	'rewrite'			=> true,
];
register_taxonomy( 'job-tax', 'job', $tax_args );