<?php
$labels = [
	'name'                => __( 'Sponsors', 'mod' ),
	'singular_name'       => __( 'Sponsor', 'mod' ),
	'add_new'             => _x( 'Add Sponsor', 'mod', 'mod' ),
	'add_new_item'        => __( 'Add Sponsor', 'mod' ),
	'edit_item'           => __( 'Edit Sponsor', 'mod' ),
	'new_item'            => __( 'Add Sponsor', 'mod' ),
	'view_item'           => __( 'View Sponsor', 'mod' ),
	'search_items'        => __( 'Search Sponsors', 'mod' ),
	'not_found'           => __( 'No Sponsors found', 'mod' ),
	'not_found_in_trash'  => __( 'No Sponsors found in Trash', 'mod' ),
	'parent_item_colon'   => __( 'Parent Sponsor:', 'mod' ),
	'menu_name'           => __( 'Sponsors', 'mod' ),
];

$args = [
	'labels'              => $labels,
	'hierarchical'        => false,
	'description'         => 'Sponsors.',
	'taxonomies'          => [ 'sponsor-tax' ],
	'public'              => false,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'show_in_rest'		  => true,
	'menu_position'       => null,
	'menu_icon'           => 'dashicons-money-alt',
	'show_in_nav_menus'   => true,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'has_archive'         => true,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => [ 'title', 'thumbnail' ]
];

register_post_type( 'sponsor', $args );

// setup taxonomies
$tax_labels = [
	'name' 				=> _x( 'Sponsor Categories', 'mod' ),
	'singular_name' 	=> _x( 'Sponsor Category', 'mod' ),
	'search_items' 		=> __( 'Search Sponsor Categories', 'mod' ),
	'all_items' 		=> __( 'All Sponsor Categories', 'mod' ),
	'edit_item' 		=> __( 'Edit Sponsor Category', 'mod' ),
	'update_item' 		=> __( 'Update Sponsor Category', 'mod' ),
	'add_new_item' 		=> __( 'Add Sponsor Category', 'mod' ),
	'new_item_name' 	=> __( 'Create Sponsor Category', 'mod' ),
	'menu_name' 		=> __( 'Sponsor Categories', 'mod' ),
	'parent_item'		=> __( 'Category Parent', 'mod' ),
];

$tax_args = [
	'hierarchical' 	    => true,
	'labels' 	    	=> $tax_labels,
	'show_ui' 	    	=> true,
	'show_admin_column' => true,
	'has_archive'		=> false,
	'query_var'	    	=> true,
	'show_in_rest'		=> true,
	'rewrite'			=> true,
];
register_taxonomy( 'sponsor-tax', 'sponsor', $tax_args );