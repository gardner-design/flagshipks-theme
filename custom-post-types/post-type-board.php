<?php
$labels = [
	'name'                => __( 'Board of Directors', 'mod' ),
	'singular_name'       => __( 'Board of Directors', 'mod' ),
	'add_new'             => _x( 'Add Board of Director', 'mod', 'mod' ),
	'add_new_item'        => __( 'Add Board of Director', 'mod' ),
	'edit_item'           => __( 'Edit Board of Director', 'mod' ),
	'new_item'            => __( 'Add Board of Director', 'mod' ),
	'view_item'           => __( 'View Board of Director', 'mod' ),
	'search_items'        => __( 'Search Board of Directors', 'mod' ),
	'not_found'           => __( 'No Board of Directors found', 'mod' ),
	'not_found_in_trash'  => __( 'No Board of Directors found in Trash', 'mod' ),
	'parent_item_colon'   => __( 'Parent Board of Director:', 'mod' ),
	'menu_name'           => __( 'Board', 'mod' ),
];

$args = [
	'labels'              => $labels,
	'hierarchical'        => false,
	'description'         => 'Board of directors.',
	'taxonomies'          => [ 'board-tax' ],
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'show_in_rest'		  => true,
	'menu_position'       => null,
	'menu_icon'           => 'dashicons-businessman',
	'show_in_nav_menus'   => true,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'has_archive'         => false,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => [ 'title', 'editor', 'thumbnail', 'page-attributes' ]
];

register_post_type( 'board', $args );

// setup taxonomies
$tax_labels = [
	'name' 				=> _x( 'Board Categories', 'mod' ),
	'singular_name' 	=> _x( 'Board Category', 'mod' ),
	'search_items' 		=> __( 'Search Board Categories', 'mod' ),
	'all_items' 		=> __( 'All Board Categories', 'mod' ),
	'edit_item' 		=> __( 'Edit Board Category', 'mod' ),
	'update_item' 		=> __( 'Update Board Category', 'mod' ),
	'add_new_item' 		=> __( 'Add Board Category', 'mod' ),
	'new_item_name' 	=> __( 'Create Board Category', 'mod' ),
	'menu_name' 		=> __( 'Board Categories', 'mod' ),
	'parent_item'		=> __( 'Board Parent', 'mod' ),
];

$tax_args = [
	'hierarchical' 	    => true,
	'labels' 	    	=> $tax_labels,
	'show_ui' 	    	=> true,
	'show_admin_column' => true,
	'has_archive'		=> false,
	'query_var'	    	=> true,
	'show_in_rest'		=> true,
	'rewrite'			=> true,
];
register_taxonomy( 'board-tax', 'board', $tax_args );