<?php
/**
 * Template Name: News Archive
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$context['news_tax'] = Timber::get_terms( ['taxonomies' => 'news-tax'] );

$context['news_posts'] = Timber::get_posts([
	'post_type' => 'news',
	'posts_per_page' => -1,
	'orderby' => 'date',
	'order' => 'DESC',
	'facetwp' => true
]);

$templates = ['news-archive.twig'];

Timber::render( $templates, $context );